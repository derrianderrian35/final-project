<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AnggotaController extends Controller
{
    public function create()
    {
        return view('anggota.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
                'nama' => 'required',
                'alamat' => 'required',
                'jk' => 'required',
                'hp' => 'required'
            ] );

            DB::table('anggota')->insert([
                'nama' => $request['nama'],
                'alamat' => $request['alamat'],
                'jk'  => $request['jk'],
                'hp'  => $request['hp']
            ]);

        return redirect('/anggota');
    }

    public function index()
    {
        $anggota = DB::table('anggota')->get(); 
        // dd($cast);
        return view('anggota.tampil', ['anggota' => $anggota]);

    }

    public function show($id)
    {
        $anggota = DB::table('anggota')->where('id', $id)->first();

        return view('anggota.detail', ['anggota' => $anggota]);
    }

    public function edit($id)
    {
        $anggota = DB::table('anggota')->where('id', $id)->first();

       return view('anggota.edit', ['anggota' => $anggota]);
    }

    public function update(Request $request, $id)
    {
        DB::table('anggota')
            ->where('id', $id)
            ->update(
                ['nama' => $request->nama,
                'alamat' => $request->alamat,
                'jk' => $request->jk,
                'hp' => $request->hp]
        );
        return redirect('/anggota');
    }

    public function destroy($id)
    {
        DB::table('anggota')->where('id', $id)->delete();

        return redirect('/anggota');
    }
}
