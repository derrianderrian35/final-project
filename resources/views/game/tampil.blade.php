@extends('layouts.master')

@section('judul')

Halaman List Game
    
@endsection

@section('content')
    <a href="/game/create" class="btn btn-primary btn-sm mb-3">
        Tambah
    </a>

    <div class="card">
    
        <!-- /.card-header -->
        <div class="card-body">
          <table id="myTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Gamplay</th>
                <th>Developer</th>
                <th>Year</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($game as $key => $value)
                   <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->gameplay}}</td>
                    <td>{{$value->developer}}</td>
                    <td>{{$value->year}}</td>
                    <td>
                        <form action="/game/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/game/{{$value->id}}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/game/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                   </tr>
                @empty
                    <p>No Data</p>
                @endforelse
            </tbody>
          </table>
        </div>
@endsection